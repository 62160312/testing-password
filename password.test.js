const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})
describe('Test Digit', () => {
  test('should has ditgit to be true', () => {
    expect(checkDigit('11111Bb#')).toBe(true)
  })
  test('should has not ditgit  in password', () => {
    expect(checkDigit('abcdefgh')).toBe(false)
  })
  test('should has not ditgit M in password', () => {
    expect(checkDigit('abcdefg.')).toBe(false)
  })
  test('should 7 charecter to be false', () => {
    expect(checkDigit('1234Bb#')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password to be true', () => {
    expect(checkSymbol('11111Bb#')).toBe(true)
  })
  test('should has number in password to be false', () => {
    expect(checkSymbol('11111111')).toBe(false)
  })
  test('should has not have  symbol in password to be false', () => {
    expect(checkSymbol('111111Aa')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password abc@11 to be false', () => {
    expect(checkPassword('abc@11')).toBe(false)
  })
})
